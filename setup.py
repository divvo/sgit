# -*- coding: utf-8 -*-
from setuptools import setup

setup(
    name='sgit',
    version='0.1',
    py_modules=['sgit'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        sgit=sgit:cli
    ''',
)
