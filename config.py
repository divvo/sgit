# -*- coding: utf-8 -*-

MASTER_BRANCH = "master"
DEVELOP_BRANCH = "develop"
FEATURE_PREFIX = "feature/"
RELEASE_PREFIX = "release/"
HOTFIX_PREFIX = "hotfix/"