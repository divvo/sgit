# -*- coding: utf-8 -*-

import click
from sh import git
from config import DEVELOP_BRANCH, MASTER_BRANCH
from utils import get_feature_name, get_release_name, get_hfix_name

@click.group()
def cli():
    pass

@cli.command()
@click.argument('name')
def create_feature(name):
    feature_name = get_feature_name(name)
    git.checkout("-b", feature_name, DEVELOP_BRANCH)

@cli.command()
@click.argument('name')
def finish_feature(name):
    feature_name = get_feature_name(name)
    git.checkout(DEVELOP_BRANCH)
    git.merge("--no-ff", feature_name)
    git.push("origin", DEVELOP_BRANCH)
    print "You must delete the feature branch manually."

@cli.command()
@click.argument('name')
def create_release(name):
    rel_name = get_release_name(name)
    git.checkout("-b", rel_name, DEVELOP_BRANCH)

@cli.command()
@click.argument('name')
def finish_release(name):
    rel_name = get_release_name(name)
    # Merge into master
    git.checkout(MASTER_BRANCH)
    git.merge("--no-ff", rel_name)
    # Tag release
    git.tag("-a", name)
    # Merge into develop
    git.checkout(DEVELOP_BRANCH)
    print "You must delete the branch manually."

@cli.command()
@click.argument('name')
def create_hotfix(name):
    hname = get_hfix_name(name)
    git.checkout("-b", hname, MASTER_BRANCH)

@cli.command()
@click.argument('name')
@click.option("--merge-dest", default=None, help="Name of secondary merge, "
                                                 "defaults to dev branch")
def finish_hotfix(name, merge_dest):
    if merge_dest is None:
        merge_dest = DEVELOP_BRANCH
    hname = get_hfix_name(name)
    # Merge into master
    git.checkout(MASTER_BRANCH)
    git.merge("--no-ff", hname)
    git.tag("-a", name)
    # Merge into develop
    git.checkout(merge_dest)
    git.merge("--no-ff", hname)
    print "You must delete the branch manually."