# -*- coding: utf-8 -*-
from config import FEATURE_PREFIX, RELEASE_PREFIX, HOTFIX_PREFIX

def get_feature_name(name):
    return FEATURE_PREFIX + name

def get_release_name(name):
    return RELEASE_PREFIX + name

def get_hfix_name(name):
    return HOTFIX_PREFIX + name